@echo off

set python=c:\Python27\python.exe
set pyinstaller=c:\Python27\pyinstaller-2.0\pyinstaller.py
set output=.\build
set bin=%output%\dist\hunter.exe
set input=.\hunter.py

if exist %bin% del /q %bin%


echo ************************************
echo Building EXE ...
%python% %pyinstaller% --onefile --out=%output% --console %input% 2>build_log.txt
echo Complete
GOTO :VERIFY



:VERIFY
if exist %bin% echo ********* Build Successful ********* && echo.
if not exist %bin% echo ********* Build Failed ********* && type build_error.txt && echo. && echo ********* Build Failed ********* && echo.


:END
pause

import sys, getopt
from modules import core


### TODO:
### Search for interesting file names
### Search for keywords in files
### Search registry
### Search known file locations for interesting stuff
### create exclude list
### Services and search for specific services (Dameware, vnc, pcanywhere, rdp

### Search ideas
# none default directory
# temp directory
# user's directory (local admins)
# find OS tools in other locations (patch dirs)
# pstools, stunnel, other unexpected tools

def main(argv):

    # Initialize searchbot, Set search file extenstion search and create filelist to process
    searchbot = core.search()

    # Parse input and set initial configuration
   
    rootdir = ''
    extlist = ''
    keywords = ''
    resultBufferSize = ''
    maxFileSizeMB = ''

    try:
        opts, args = getopt.getopt(argv,"r:e:b:k:m:h")
        if len(argv) == 0:
            print searchbot.banner()
            print searchbot.usage()
            sys.exit(2)
        
    except getopt.GetoptError:
        print searchbot.banner()
        print searchbot.usage()
        sys.exit(2)

    for opt, arg in opts:

        if opt == '-h':
            print searchbot.banner()
            print searchbot.usage()
            sys.exit()
        elif opt in ("-r"):
            searchbot.rootdirs = []
            for item in arg.split(','):
                searchbot.rootdirs.append(item)
        elif opt in ("-e"):
             searchbot.extlist = []
             for item in arg.split(','):
                 searchbot.extlist.append(item)
        elif opt in ("-b"):
             searchbot.resultBufferSize = int(arg)
        elif opt in ("-k"):
             searchbot.keywords = []
             for item in arg.split(','):
                 searchbot.keywords.append(item)
        elif opt in ("-m"):
             try:
                 searchbot.maxFileSizeMB = int(arg)
             except:
                 print searchbot.banner()
                 print searchbot.usage()
                 print "[ERROR] Max filesize must be integer in MB\n"
                 sys.exit()

    if searchbot.rootdirs == []:
        print searchbot.banner()
        print searchbot.usage()
        print "[ERROR] Root directory must contain something to search"
        sys.exit()




    # Create file list based on file extension and begin search
    print searchbot.banner()
    searchbot.buildFileList()
    searchbot.processFiles()
    searchbot.searchFileList()
    searchbot.interestingFilesRecentlyOpened()
    searchbot.interestingFilesNetworkShortcuts()
        
###################################
## Start App

if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        sys.exit(0)       







# -*- mode: python -*-
a = Analysis(['.\\hunter.py'],
             pathex=['Z:\\development\\projects\\hunter'],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=os.path.join('dist', 'hunter.exe'),
          debug=False,
          strip=None,
          upx=True,
          console=True )

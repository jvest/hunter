import os
from zipfile import ZipFile
from os.path import join



####################### Search Class #######################
class search:
    def __init__(self):
        self.rootdirs = []
        self.extlist = ["", ".cfg", ".txt", ".ini", ".xml", ".doc", ".xls", ".docx", ".xlsx", ".config"]
        self.keywords = ["swords","swords.","sword.","sword ", "sword:", "sword=","admin ", "admin:", "admin=", "root ", "root=","root:"]
        self.maxFileSizeMB = 5
        self.resultBufferSize = 30
        self.filelist = [] 
        self.userroot = ''
        
        #Job Processor type dictionary
        self.jobTypesDict = {
                        '.doc':'BINARY',
                        '.docx':'MICROSOFT.X',
                        '.xls':'BINARY',
                        '.xlsx':'MICROSOFT.X',
                        '.ppt':'MICROSOFT',
                        '.pptx':'MICROSOFT.X',
                        }
        
        description = "Recursive search of files based on file extension"
      

    def convertMBtoBytes(self,size):
        return size * 1024 * 1024
        
    
    # Create list of all files with specified extenstion
    def buildFileList(self):
        for rootdir in self.rootdirs:
            for folder, subs, files in os.walk(rootdir):
                for f in files:
                    if os.path.splitext(f)[-1].lower() in self.extlist:
                        try:
                            if os.path.getsize(os.path.join(folder,f)) < self.convertMBtoBytes(self.maxFileSizeMB):    
                                self.filelist.append(os.path.join(folder,f))
                        except:
                            pass
        
        print str(len(self.filelist)) + " files found to be analyzed."
        
    def processFiles(self):
        print "*" * 80 + "\n"
        print "Keyword search on file content"
        print "=" * 40
        for job in self.filelist:        
            fileName, fileExtension = os.path.splitext(job)
            # Determine how to process job
            jobtype = self.getJobProcessor(fileExtension)
            #print jobtype
            if jobtype == 'MICROSOFT.X': 
                # Process as MICROSOFT job
                self.processMicrosoftDotXFile(job)
            elif jobtype == "BINARY": 
                # Process as BINARY job
                self.processBinaryFile(job)
            else: # Process as PLAINTEXT job
                self.processPlainTextFile(job)
                 

    def getJobProcessor(self,fileExt):
        result = self.jobTypesDict.get(fileExt, 'PLAINTEXT')
        return result
            
  
    def processMicrosoftDotXFile(self,job):
        #Reference: http://stackoverflow.com/questions/116139/how-can-i-read-a-word-2007-docx-file
             
        try:
            count = 1
            z = ZipFile(job)
            #print z.namelist()
            for item in z.namelist():
                content = z.read(item)
                for keyword in self.keywords:
                    if keyword.lower() in content.lower():
                        
                        startidx = content.find(keyword) - self.resultBufferSize
                        if startidx < 0:
                            startidx = 0
                        endidx = startidx + len(keyword) + (self.resultBufferSize*2)
                        result = content.rstrip('\n')[startidx:endidx]
                        
                        print "(" + str(count) + ":" + keyword + ":" + job + ") " +  result
            count = count + 1
            z.close()
        except:
            print "Cannot read", job

    def processPlainTextFile(self,job):
         
        try:
            count = 1            
            currentfile = open(job, 'r')
            while 1:                    
                line = currentfile.readline()
                #print line
                if not line:
                    break
                for keyword in self.keywords:
                    if keyword.lower() in line.lower():
                        
                        startidx = line.find(keyword) - self.resultBufferSize
                        if startidx < 0:
                            startidx = 0
                        endidx = startidx + len(keyword) + (self.resultBufferSize*2)
                        result = line.rstrip('\n')[startidx:endidx]
                        
                        print "(" + str(count) + ":" + keyword + ":" + job + ") " +  result
                count = count + 1
            
            currentfile.close()
        except:
            print "Cannot read", job 

    def setUserRoot(self):
        if os.path.isdir('c:\\users'):
            self.userroot = 'c:\\users'
        elif os.path.isdir('c:\\Documents and Settings'):
            self.userroot = 'c:\\Documents and Settings'
        elif os.path.isdir('d:\\users'):
            self.userroot = 'd:\\users'
        elif os.path.isdir('d:\\Documents and Settings'):
            self.userroot = 'd:\\Documents and Settings'
        elif os.path.isdir('e:\\users'):
            self.userroot = 'e:\\users'
        elif os.path.isdir('e:\\Documents and Settings'):
            self.userroot = 'e:\\Documents and Settings'
        elif os.path.isdir('f:\\users'):
            self.userroot = 'f:\\users'
        elif os.path.isdir('f:\\Documents and Settings'):
            self.userroot = 'f:\\Documents and Settings'
        else:
            self.userroot = ''


    def interestingFilesRecentlyOpened(self):
        self.setUserRoot()
        #Display recent files for each user
        print "*" * 80 + "\n"
        print "Recenlty opened files"
        print "=" * 40
        # Get user list
        users = [d for d in os.listdir(self.userroot) if os.path.isdir(self.userroot + "\\" + d)]
        #print userroot, users
        for user in users:
            print self.userroot + "\\" + user
            try:
                recentItems = os.listdir(self.userroot + '\\' + user + "\\AppData\\Roaming\\Microsoft\\Windows\\Recent")
                for item in recentItems:
                    print "\t" + item
            except: 
                print '\tAccess denied for ' + user + "\n"

    def interestingFilesNetworkShortcuts(self):
        self.setUserRoot()
        #Display network shortcuts for each user
        print "*" * 80 + "\n"
        print "Network shortcuts"
        print "=" * 40
        # Get user list
        users = [d for d in os.listdir(self.userroot) if os.path.isdir(self.userroot + "\\" + d)]
        #print userroot, users
        for user in users:
            print self.userroot + "\\" + user
            try:
                recentItems = os.listdir(self.userroot + '\\' + user + "\\AppData\Roaming\Microsoft\Windows\Network Shortcuts")
                for item in recentItems:
                    print "\t" + item
            except: 
                print '\tAccess denied for ' + user + "\n"


    def removeNonAscii(self,s): return "".join(i for i in s if (ord(i)<128) and (ord(i)>31))

    def processBinaryFile(self,job):
        try:        
            count = 1            
            currentfile = open(job, 'rb')
            completeFile = self.removeNonAscii(currentfile.read())
            #print completeFile
            for keyword in self.keywords:
                if keyword.lower() in completeFile.lower():
                    
                    startidx = completeFile.find(keyword) - self.resultBufferSize
                    if startidx < 0:
                        startidx = 0
                    endidx = startidx + len(keyword) + (self.resultBufferSize*2)
                    result = completeFile.rstrip('\n')[startidx - self.resultBufferSize:endidx]

                    print "(" + str(count) + ":" + keyword + ":" + job + ") " +  result
            count = count + 1
            
            currentfile.close()

        except:
            print "Cannot read", job

    def searchFileList(self):
        print "*" * 80 + "\n"
        print "Keyword search on filename"
        print "=" * 40
        for filename in self.filelist:
            #print filename
            for keyword in self.keywords:
                if keyword.lower() in filename.lower():
                    print keyword + ":" + filename
 
    def banner(self):
        output =  "\n"
        output += "Hunter v0.1" + "\n"
        output += "Author: Joe Vest" + "\n"
        output += "=" * 80 + "\n"
        output += "Window file system search tool\n\n"
        output += "Parameter Settings\n"
        output += " Root Directory(s)  \t" +', '.join(self.rootdirs) + "\n"
        output += " Extension List  \t" + ', '.join(self.extlist) + "\n"
        output += " Keywords        \t" + ', '.join(self.keywords) + "\n"
        output += " Max Filesize(MB)\t" + str(self.maxFileSizeMB) + "\n"
        output += " Buffer Size     \t" + str(self.resultBufferSize) + "\n"
        return output

    def usage(self):
        output =  ""
        output += "HUNTER [-r Root directory(s)] [-e File extenstion list]\n"
        output += "         [-k Search keywords]   [-m Max filesize to search]\n"
        output += "         [-b Charater buffer]   [-h]\n"
        output += "  -r\tRoot Directory(s) to start search (comma separated)\n"
        output += "  -e\tFile exetentions to search (comma separated)\n"
        output += "  -k\tKeywords to search (comma separated)\n"
        output += "  -m\tMax filesize to search in MB\n"
        output += "  -b\tNumber of characters to display before and after keyword\n"
        output += "  -h\tThis Help\n\n"
        output += "Example:\n"
        output += '  hunter -r c:/users'
        output += '  hunter -r c:/users,"d:/some directory" -e .doc,.txt -k sword,stuff -m 10 -b 25'
        output += "\n\n"
        output += "Spaces, Quotes and Double quotes in search terms\n"
        output += "Use double qoutes around directory or keyword values to include a space\n"
        output += "Example:\n"
        output += '"' + "i have spaces" + '"' + ',' + "idonthavespaces\n"
        output += "\n"
        output += "Double quotes may also be used to quote other double quotes or single quotes.\n"
        output += "The search term entered as " + '"""' + "abc" + " will search for " + '"' + "abc\n"
        return output 
   





